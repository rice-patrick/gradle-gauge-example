package org.singleservingsoftware.hook;

import java.util.logging.Logger;

import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.ExecutionContext;
import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.datastore.ScenarioDataStore;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.singleservingsoftware.cache.DriverCache;

public class SeleniumHook {

    private static Logger logger = Logger.getLogger(SeleniumHook.class.getName());

    /**
     * Stores the executionContext of the current test within the scenario store. This allows other tests to access
     * it easily. Note that this runs on _all_ tests, not just selenium tests.
     * @param context
     */
    @BeforeScenario()
    public void setupContext(ExecutionContext context)
    {
        //Store the context in the scenario store for lazy loading later on
        ScenarioDataStore.put("context", context);

        //The rest of the driver will be lazy loaded on first use - see DriverCache.getDriver()
    }

    /**
     * Runs after selenium tests to store the results of the test if running in SauceLabs. Limited to running on tests
     * tagged with _selenium_
     * @param context
     */
    @AfterScenario(tags = {"selenium"})
    public void teardownSeleniumDriver(ExecutionContext context) {

        if(DriverCache.getDriver() == null)
        {
            String s = "No driver was present when closing the test. If using a remote driver, test" +
                    "status may not be persisted correctly! This is likely a coding error";

            //Log this in several places (in the logs, and in the report
            logger.warning(s);
            Gauge.writeMessage(s);
        }
        else if(DriverCache.getDriver() != null) {
            //Print job information if we are running remotely (on saucelabs)
            handleRemoteJobEnd(context);

            //Close the driver
            DriverCache.getDriver().quit();
        }    
    }

    private void handleRemoteJobEnd(ExecutionContext context) {
        //If we're not a remote webdriver, exit early.
        if(!DriverCache.getDriver().getClass().isAssignableFrom(RemoteWebDriver.class))
            return;
        
        String jobStatus = context.getCurrentScenario().getIsFailing() ? "failed" : "passed";
        ((JavascriptExecutor) DriverCache.getDriver()).executeScript("sauce:job-result=" + jobStatus);

        if(DriverCache.getSessionID() != null && jobStatus.equals("failed"))
            Gauge.writeMessage(
                    String.format("Sauce URL: https://app.saucelabs.com/tests/%s", DriverCache.getSessionID()));
    }

    


}
