package org.singleservingsoftware.hook;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.ExecutionContext;
import com.thoughtworks.gauge.datastore.ScenarioDataStore;

/**
 * This class just adds the scenario and specification name to a scenario-level datastore for every test,
 * so that they can be retrieved later if necessary for creating unique testing data.
 */
public class SpecNameHook {

    /**
     * This runs before every scenario, and adds data to the spec store.
     * @param context
     */
    @BeforeScenario
    public void addScenarioName(ExecutionContext context) {

        ScenarioDataStore.put("scenarioName", context.getCurrentScenario().getName());
        ScenarioDataStore.put("specificationName", context.getCurrentSpecification().getName());

    }
}
